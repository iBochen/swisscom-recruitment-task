# Swisscom recruitment assessment

Web application that allows users to manage feature toggles for their
applications.

User interface allows for adding, updating and archiving toggle features.

REST API allows for getting information what features are ON and OFF for a specific customer.

All data is stored in MongoDB database on MongoDB Atlas private server.

## How to run

Server side is Spring Boot application. REST service can can be listened on port 8080 and run by using command:

```bash
mvn spring-boot:run
```

Client app is visible on address http://localhost:4200/ and can be run using command:

```bash
ng serve
```

## REST API
```bash
POST /api/v1/features
{
 "featureRequest": {
 "customerId": "1234",
 "features": [
 {"name": "my-feature-a"},
 {"name": "my-feature-b"},
 {"name": "my-feature-c"},
 {"name": "my-feature-d"}
 }
 }
}
```

Example API response:
```bash
{
 "features": [
 {"name": "my-feature-a", "active": true, "inverted": false, "expired": false},
 {"name": "my-feature-b", "active": false, "inverted": true, "expired": false},
 {"name": "my-feature-c", "active": false, "inverted": false, "expired": false},
 {"name": "my-feature-d", "active": true, "inverted": false, "expired": true}
 ]
}
```

## Technology stack
Backend language - Java 11, 
Frontend language - Angular 9, 
Persistence layer - MongoDB.

Libraries - Angular Material, lombok, maven, rxjs, junit.


## Author
Łukasz Bocheński
