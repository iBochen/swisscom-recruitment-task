package com.swisscom.recruitment.controller;

import com.swisscom.recruitment.model.ToggleFeatureDto;
import com.swisscom.recruitment.service.ToggleFeatureService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ToggleFeatureControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    ToggleFeatureService service;

    @Test
    public void testGetRequest_getAllToggleFeatures_shouldReturnOkStatus() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/api/v1/features"))
                .andExpect(status().isOk()).andDo(print());
    }

    @Test
    public void testGetRequest_getToggleFeatureByTechnicalName_shouldReturnOkStatus() throws Exception {
        String name = "my-feature-a";
        ToggleFeatureDto dto = ToggleFeatureDto.builder().technicalName(name).build();
        ResponseEntity<ToggleFeatureDto> response = ResponseEntity.ok().body(dto);
        when(service.getToggleFeatureByTechnicalName(name)).thenReturn(response);
        this.mvc.perform(MockMvcRequestBuilders.get("/api/v1/features/"+name))
                .andExpect(status().isOk()).andDo(print())
                .andExpect(content().string(containsString(name)));
    }

    @Test
    public void testDeleteRequest_deleteToggleFeature_shouldReturnNotFoundStatus() throws Exception {
        String name = "not-existing-feature";
        when(service.deleteToggleFeature(name)).thenReturn(ResponseEntity.notFound().build());
        this.mvc.perform(MockMvcRequestBuilders.delete("/api/v1/features/"+name))
                .andExpect(status().isNotFound()).andDo(print());
    }
}
