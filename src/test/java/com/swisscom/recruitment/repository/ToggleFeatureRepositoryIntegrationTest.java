package com.swisscom.recruitment.repository;

import com.swisscom.recruitment.model.FeatureRequestDto;
import com.swisscom.recruitment.model.ToggleFeature;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
@ExtendWith(SpringExtension.class)
public class ToggleFeatureRepositoryIntegrationTest {

    @Autowired
    private ToggleFeatureRepository repository;

    private MongodExecutable mongodExecutable;

    @BeforeEach
    public void setUp() throws IOException {
        String ip = "localhost";
        int port = 27017;

        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
                .net(new Net(ip, port, Network.localhostIsIPv6()))
                .build();

        MongodStarter starter = MongodStarter.getDefaultInstance();

        mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();


        List<ToggleFeature> featureList = new ArrayList<>();
        ToggleFeature feature = ToggleFeature.builder().displayName("First feature").technicalName("my-feature-a").expiresOn(LocalDateTime.now())
                .description("Test data for usage").inverted(false).customerIds(new String[]{"123456","02468"}).build();
        ToggleFeature feature2 = ToggleFeature.builder().displayName("Second feature").technicalName("my-feature-b").expiresOn(LocalDateTime.now())
                .description("Test data for usage").inverted(false).customerIds(new String[]{"123456","02468"}).build();
        ToggleFeature feature3 = ToggleFeature.builder().displayName("Third feature").technicalName("my-feature-c").expiresOn(LocalDateTime.now())
                .description("Test data for usage").inverted(false).customerIds(new String[]{"123456","02468"}).build();
        ToggleFeature feature4 = ToggleFeature.builder().displayName("Fourth feature").technicalName("my-feature-d").expiresOn(LocalDateTime.now())
                .description("Test data for usage").inverted(false).customerIds(new String[]{"123456","02468"}).build();
        featureList.add(feature);
        featureList.add(feature2);
        featureList.add(feature3);
        featureList.add(feature4);

        repository.saveAll(featureList);
    }

    @AfterEach
    void tearDown() {
        repository.deleteAll();
        mongodExecutable.stop();
    }

    @Test
    public void testFindByTechnicalName(){
        String name = "my-feature-a";
        ToggleFeature result = repository.findByTechnicalName(name);
        assertThat(result).hasFieldOrProperty("technicalName");
    }

    @Test
    public void testFindAllByTechnicalNameIn(){
        String consumerId = "123456";
        List<String> featureNames = new ArrayList<>();
        featureNames.add("my-feature-a");
        featureNames.add("my-feature-b");
        FeatureRequestDto featureRequestDto = FeatureRequestDto.builder().build();
        featureRequestDto.setFeatureRequest(consumerId, featureNames);
        List<ToggleFeature> result = repository.findAllByTechnicalNameIn(featureNames);

        assertThat(result).hasSize(2);
    }

}
