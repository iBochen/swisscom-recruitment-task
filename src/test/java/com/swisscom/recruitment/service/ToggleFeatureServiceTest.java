package com.swisscom.recruitment.service;

import com.swisscom.recruitment.model.*;
import com.swisscom.recruitment.repository.ToggleFeatureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class ToggleFeatureServiceTest {

    @Autowired
    ToggleFeatureService service;

    @MockBean
    ToggleFeatureRepository repository;

    private List<ToggleFeature> mockEntities;

    @BeforeEach
    public void setUp() {
        mockEntities = new ArrayList<>();
        mockEntities.add(ToggleFeature.builder().displayName("First feature").technicalName("my-feature-a").build());
        mockEntities.add(ToggleFeature.builder().displayName("Second feature").technicalName("my-feature-b").build());
        mockEntities.add(ToggleFeature.builder().displayName("Third feature").technicalName("my-feature-c").build());
        mockEntities.add(ToggleFeature.builder().displayName("Fourth feature").technicalName("my-feature-d").build());
    }

    @Test
    void testGetAllToggleFeatures(){
        when(repository.findAll()).thenReturn(mockEntities);
        ResponseEntity<List<ToggleFeatureDto>> result = service.getAllToggleFeatures();
        assertEquals(HttpStatus.OK,result.getStatusCode());
        assertEquals(4, Objects.requireNonNull(result.getBody()).size());
    }

    @Test
    void testGetToggleFeatureByTechnicalName(){
        String name = "my-feature-a";
        ToggleFeature response = ToggleFeature.builder().technicalName(name).build();
        when(repository.findByTechnicalName(name)).thenReturn(response);

        ResponseEntity<ToggleFeatureDto> result = service.getToggleFeatureByTechnicalName(name);
        assertEquals(HttpStatus.OK,result.getStatusCode());
        assertEquals(name, Objects.requireNonNull(result.getBody()).getTechnicalName());
    }

    @Test
    void testDeleteToggleFeature_shouldReturnOkStatus(){
        String name = "my-feature-a";
        ToggleFeature response = ToggleFeature.builder().technicalName(name).build();
        when(repository.findByTechnicalName(name)).thenReturn(response);

        ResponseEntity<?> result = service.deleteToggleFeature(name);
        assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    void testDeleteToggleFeature_shouldReturnNotFoundStatus(){
        String name = "not-existing-feature";

        ResponseEntity<?> result = service.deleteToggleFeature(name);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }
}
