package com.swisscom.recruitment.controller;

import com.swisscom.recruitment.model.FeatureRequestDto;
import com.swisscom.recruitment.model.FeatureResponseDto;
import com.swisscom.recruitment.model.ToggleFeatureDto;
import com.swisscom.recruitment.service.ToggleFeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin("*")
public class ToggleFeatureController {

    private final ToggleFeatureService toggleFeatureService;

    @Autowired
    ToggleFeatureController(ToggleFeatureService toggleFeatureService){
        this.toggleFeatureService = toggleFeatureService;
    }

    @GetMapping(value = "/features")
    public ResponseEntity<List<ToggleFeatureDto>> getAllToggleFeatures() {
        return toggleFeatureService.getAllToggleFeatures();
    }

    @GetMapping(value = "features/{name}")
    public ResponseEntity<ToggleFeatureDto> getToggleFeatureByTechnicalName(@PathVariable("name") String name){
        return toggleFeatureService.getToggleFeatureByTechnicalName(name);
    }

    @PostMapping(value = "/features/new")
    public ResponseEntity<ToggleFeatureDto> createToggleFeature(@RequestBody ToggleFeatureDto toggleFeatureDto) {
        return toggleFeatureService.createToggleFeature(toggleFeatureDto);
    }

    @PostMapping(value = "/features")
    public ResponseEntity<List<FeatureResponseDto>> getAllToggleFeaturesByCustomerId(@RequestBody FeatureRequestDto featureRequestDto) {
        return toggleFeatureService.getAllToggleFeaturesByCustomerId(featureRequestDto);
    }

    @PutMapping(value = "/features/{name}")
    public ResponseEntity<ToggleFeatureDto> updateToggleFeature(@PathVariable("name") String name,
                                                                   @RequestBody ToggleFeatureDto toggleFeatureDto) {
        return toggleFeatureService.updateToggleFeature(name, toggleFeatureDto);
    }

    @DeleteMapping(value = "/features/{name}")
    public ResponseEntity<?> deleteToggleFeature(@PathVariable("name") String name) {
        return toggleFeatureService.deleteToggleFeature(name);
    }

}
