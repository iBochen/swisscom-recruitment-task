package com.swisscom.recruitment.repository;

import com.swisscom.recruitment.model.ToggleFeature;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToggleFeatureRepository extends MongoRepository<ToggleFeature,String> {

    ToggleFeature findByTechnicalName(String technicalName);

    List<ToggleFeature> findAllByTechnicalNameIn(List<String> features);
}
