package com.swisscom.recruitment.converter;

import com.swisscom.recruitment.model.ToggleFeatureDto;
import com.swisscom.recruitment.model.ToggleFeature;
import org.springframework.stereotype.Component;

@Component
public class ToggleFeatureConverter extends Converter<ToggleFeatureDto, ToggleFeature> {

    public ToggleFeatureConverter(){
        super(
                feature -> ToggleFeature.builder().displayName(feature.getDisplayName())
                        .technicalName(feature.getTechnicalName()).expiresOn(feature.getExpiresOn())
                        .description(feature.getDescription()).inverted(feature.getInverted())
                        .customerIds(feature.getCustomerIds()).build(),

                featureEntity -> ToggleFeatureDto.builder().displayName(featureEntity.getDisplayName())
                        .technicalName(featureEntity.getTechnicalName()).expiresOn(featureEntity.getExpiresOn())
                        .description(featureEntity.getDescription()).inverted(featureEntity.getInverted())
                        .customerIds(featureEntity.getCustomerIds()).build());
    }
}
