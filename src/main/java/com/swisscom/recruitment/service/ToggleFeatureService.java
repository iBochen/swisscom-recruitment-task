package com.swisscom.recruitment.service;

import com.swisscom.recruitment.converter.ToggleFeatureConverter;
import com.swisscom.recruitment.model.*;
import com.swisscom.recruitment.repository.ToggleFeatureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ToggleFeatureService {

    private final ToggleFeatureRepository toggleFeatureRepository;
    private final ToggleFeatureConverter toggleFeatureConverter;

    @Autowired
    ToggleFeatureService(ToggleFeatureRepository repository,ToggleFeatureConverter converter){
        this.toggleFeatureRepository = repository;
        this.toggleFeatureConverter = converter;
    }

    public ResponseEntity<List<ToggleFeatureDto>> getAllToggleFeatures() {
        return Optional.of(toggleFeatureRepository.findAll())
                .map(entities -> ResponseEntity.ok().body(toggleFeatureConverter.createFromEntities(entities)))
                .orElseGet(() ->ResponseEntity.noContent().build());
    }

    public ResponseEntity<ToggleFeatureDto> getToggleFeatureByTechnicalName(String name) {
        return Optional.ofNullable(toggleFeatureRepository.findByTechnicalName(name))
                .map(toggleFeature -> ResponseEntity.ok().body(toggleFeatureConverter.convertFromEntity(toggleFeature)))
                .orElseGet(() ->ResponseEntity.notFound().build());
    }

    public ResponseEntity<ToggleFeatureDto> createToggleFeature(ToggleFeatureDto toggleFeatureDto) {
        ToggleFeature toggleFeature = toggleFeatureRepository.insert(toggleFeatureConverter.convertFromDto(toggleFeatureDto));
        return Optional.of(toggleFeature).map(entity -> ResponseEntity.ok().body(toggleFeatureConverter.convertFromEntity(entity)))
                .orElseGet(() ->ResponseEntity.status(HttpStatus.CONFLICT).build());
    }

    public ResponseEntity<List<FeatureResponseDto>> getAllToggleFeaturesByCustomerId(FeatureRequestDto featureRequestDto) {
        String customerId = featureRequestDto.getCustomerId();
        List<String> features = featureRequestDto.getFeatureNames();
        List<ToggleFeature> featureEntities = toggleFeatureRepository.findAllByTechnicalNameIn(features);
        List<FeatureResponseDto> responses = getFeatureResponseDtos(customerId, featureEntities);
        return Optional.of(featureEntities)
                .map(entities -> ResponseEntity.ok().body(responses))
                .orElseGet(() ->ResponseEntity.noContent().build());
    }

    private List<FeatureResponseDto> getFeatureResponseDtos(String customerId, List<ToggleFeature> featureEntities) {

        return featureEntities.stream().map(entity->{FeatureResponseDto featureResponseDto = new FeatureResponseDto();
            featureResponseDto.setName(entity.getTechnicalName());
            boolean inverted = entity.getInverted();
            featureResponseDto.setInverted(inverted);
            boolean expired = checkIfExpired(entity.getExpiresOn());
            featureResponseDto.setExpired(expired);
            boolean presented = Arrays.asList(entity.getCustomerIds()).contains(customerId);
            featureResponseDto.setActive(checkIfActive(presented,inverted,expired));

            return featureResponseDto;
        }).collect(Collectors.toList());
    }

    private boolean checkIfExpired(LocalDateTime expiresOn) {
        LocalDateTime today = LocalDateTime.now();
        return expiresOn.isBefore(today);
    }

    private boolean checkIfActive(boolean presented, boolean inverted, boolean expired) {
        if (presented){
            return !inverted && !expired;
        } else {
            return expired;
        }
    }

    public ResponseEntity<ToggleFeatureDto> updateToggleFeature(String name, ToggleFeatureDto toggleFeatureDto) {
        return Optional.ofNullable(toggleFeatureRepository.findByTechnicalName(name))
                .map(entity -> {
                    entity.setDisplayName(toggleFeatureDto.getDisplayName());
                    entity.setTechnicalName(toggleFeatureDto.getTechnicalName());
                    entity.setExpiresOn(toggleFeatureDto.getExpiresOn());
                    entity.setDescription(toggleFeatureDto.getDescription());
                    entity.setInverted(toggleFeatureDto.getInverted());
                    entity.setCustomerIds(toggleFeatureDto.getCustomerIds());
                    ToggleFeature updatedEntity = toggleFeatureRepository.save(entity);
                    return ResponseEntity.ok().body(toggleFeatureConverter.convertFromEntity(updatedEntity));
                }).orElseGet(() ->ResponseEntity.notFound().build());
    }

    public ResponseEntity<?> deleteToggleFeature(String name) {
        return Optional.ofNullable(toggleFeatureRepository.findByTechnicalName(name))
                .map(feature -> {toggleFeatureRepository.delete(feature);
                    return ResponseEntity.ok().build();
                }).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
