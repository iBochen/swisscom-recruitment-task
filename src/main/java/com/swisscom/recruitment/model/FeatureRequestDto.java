package com.swisscom.recruitment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
public class FeatureRequestDto {
    private FeatureRequest featureRequest;

    public String getCustomerId() {
        return featureRequest.getCustomerId();
    }

    public List<String> getFeatureNames(){
        return featureRequest.getFeatures().stream().map(Feature::getName).collect(Collectors.toList());
    }

    public void setFeatureRequest(String customerId,List<String> featureNames) {
        this.featureRequest = new FeatureRequest(customerId,featureNames);
    }
}

@Data
class FeatureRequest {
    private String customerId;
    private List<Feature> features;

    FeatureRequest(String customerId, List<String> featureNames){
        this.customerId = customerId;
        features = featureNames.stream().map(Feature::new).collect(Collectors.toList());
    }
}

@Data
@AllArgsConstructor
class Feature {
    private String name;
}
