package com.swisscom.recruitment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ToggleFeatureDto {
    private String displayName;
    private String technicalName;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "GMT",shape = JsonFormat.Shape.STRING)
    private LocalDateTime expiresOn;
    private String description;
    private Boolean inverted;
    private String[] customerIds;
}
