package com.swisscom.recruitment.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Builder
@Data
@Document(collection = "toggle_features")
public class ToggleFeature {

    @Id
    private String id;
    private String displayName;
    @Indexed(unique=true)
    private String technicalName;
    private LocalDateTime expiresOn;
    private String description;
    private Boolean inverted;
    private String[] customerIds;
}
