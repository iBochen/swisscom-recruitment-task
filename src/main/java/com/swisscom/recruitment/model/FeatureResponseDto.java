package com.swisscom.recruitment.model;

import lombok.Data;

@Data
public class FeatureResponseDto {
    private String name;
    private boolean active;
    private boolean inverted;
    private boolean expired;
}
