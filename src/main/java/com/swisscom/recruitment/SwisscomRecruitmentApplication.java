package com.swisscom.recruitment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwisscomRecruitmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwisscomRecruitmentApplication.class, args);
    }

}
