import {Injectable} from "@angular/core";
import {Http} from '@angular/http';
import {ToggleFeature} from "../domain/toggle-feature";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable()
export class ToggleFeatureService {
  private baseUrl = 'http://localhost:8080/api/v1/';

  constructor(private http: HttpClient) {
  }

  getAllToggleFeatures(): Observable<ToggleFeature[]> {
    return this.http.get<ToggleFeature[]>(this.baseUrl + "features")
      .pipe(catchError(this.handleError));
  }

  createToggleFeature(toggleFeature: ToggleFeature): Observable<ToggleFeature> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<ToggleFeature>(this.baseUrl + "features/new", toggleFeature, {headers: headers})
      .pipe(catchError(this.handleError));
  }

  updateToggleFeature(name: string, toggleFeature: ToggleFeature): Observable<ToggleFeature> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put<ToggleFeature>(this.baseUrl + "features/" + name, toggleFeature, {headers: headers})
      .pipe(catchError(this.handleError));
  }

  deleteToggleFeature(name: string) {
    return this.http.delete<ToggleFeature>(this.baseUrl + "features/" + name)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
