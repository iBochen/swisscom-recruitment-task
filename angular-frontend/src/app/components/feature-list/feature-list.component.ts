import {Component, OnInit, ViewChild} from "@angular/core";
import {ToggleFeature} from "../../domain/toggle-feature";
import {ToggleFeatureService} from "../../services/toggle-feature.service";
import {NewToggleFeatureComponent} from "../new-toggle-feature/new-toggle-feature.component";
import {MatDialog} from "@angular/material/dialog";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'feature-list',
  templateUrl: './feature-list.component.html',
  styleUrls: ['./feature-list.component.css']
})

export class FeatureListComponent implements OnInit {

  dataSource: MatTableDataSource<ToggleFeature>;

  displayedColumns: string[] = ['no', 'displayName', 'technicalName', 'expiresOn', 'description', 'inverted', 'customerIds', 'edit', 'archive'];
  customerIdsColumns: string[] = ['ids'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private toggleFeatureService: ToggleFeatureService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.getAllToggleFeatures();
  }

  getAllToggleFeatures(): void {
    this.toggleFeatureService.getAllToggleFeatures()
      .subscribe(feature => {
        this.dataSource = new MatTableDataSource(feature);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  deleteToggleFeature(name: string): void {
    this.toggleFeatureService.deleteToggleFeature(name).subscribe(feature => {
      this.dataSource.data = this.dataSource.data.filter(todo => todo.technicalName != name);
    });
  }

  createToggleFeature(): void {
    const dialogRef = this.dialog.open(NewToggleFeatureComponent, {
      width: '100%',
      data: {}
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {

        this.toggleFeatureService.createToggleFeature(result)
          .subscribe(feature => {
            this.dataSource.data.push(feature);
            this.dataSource.filter = "";
          });
      }
    });
  }

  updateToggleFeature(name: string, toggleFeature: ToggleFeature): void {
    const dialogRef = this.dialog.open(NewToggleFeatureComponent, {
      width: '100%',
      data: {toggleFeature: toggleFeature}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null)
        this.toggleFeatureService.updateToggleFeature(name, result).subscribe(feature => {
          let index = this.dataSource.data.findIndex(obj => obj.technicalName == name);
          this.dataSource.data[index] = feature;
          this.dataSource.filter = "";
        });
    })
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
}
