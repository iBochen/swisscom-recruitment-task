import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {ToggleFeature} from "../../domain/toggle-feature";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MatChipInputEvent} from "@angular/material/chips";
import {COMMA, ENTER} from "@angular/cdk/keycodes";

@Component({
  selector: 'app-new-toggle-feature',
  templateUrl: './new-toggle-feature.component.html',
  styleUrls: ['./new-toggle-feature.component.css']
})
export class NewToggleFeatureComponent implements OnInit {

  toggleFeature: ToggleFeature;
  label: string;

  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(public dialogRef: MatDialogRef<NewToggleFeatureComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.data.toggleFeature ?
      this.toggleFeature = this.data.toggleFeature : this.toggleFeature = new ToggleFeature();
    this.data.toggleFeature ? this.label = "UPDATE" : this.label = "CREATE";
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      if (this.toggleFeature.customerIds == null)
        this.toggleFeature.customerIds = [];
      this.toggleFeature.customerIds.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  remove(id: string): void {
    const index = this.toggleFeature.customerIds.indexOf(id);

    if (index >= 0) {
      this.toggleFeature.customerIds.splice(index, 1);
    }
  }

}
