import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewToggleFeatureComponent } from './new-toggle-feature.component';

describe('NewToggleFeatureComponent', () => {
  let component: NewToggleFeatureComponent;
  let fixture: ComponentFixture<NewToggleFeatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewToggleFeatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewToggleFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
